package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

// User : struct of the users table
type User struct {
	ID       int    `json:"id"`
	Edad     int    `json:"edad"`
	Nombre   string `json:"nombre"`
	Apellido string `json:"apellido"`
	Email    string `json:"email"`
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", saluda).Methods("GET")
	r.HandleFunc("/user/{id}", busca).Methods("GET")
	log.Fatal(http.ListenAndServe(":8000", r))

}

func saluda(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hola mundo!!.."))

}
func busca(w http.ResponseWriter, r *http.Request) {
	Vars := mux.Vars(r)
	userID := Vars["id"]
	u1 := User{
		ID:       0,
		Edad:     30,
		Nombre:   "Juan",
		Apellido: "Perez",
		Email:    "juan.Perez@gmail.com",
	}
	w.Write([]byte(userID))
	json.NewEncoder(w).Encode(u1)
}
